## About Base Laravel Project

to run this project you need to run following command:

- "composer install"
- "php artisan migrate"
- "php artisan db:seed"
- "php artisan storage:link"

make sure to create your database first before config .ENV files

Used Packages :
- Spatie Permission
- Datatables

Features :
- Account Control
- Roles & Permissions
- Users Control
- Adboxs Control
- Display Request Messages From client
- Server-side Datatables
- Export options : csv, excel, pdf, and print
- CRUD with Bulk Delete
- API Authentication with Sanctum
