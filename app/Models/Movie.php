<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    use HasFactory;
    public $incrementing = false;
    protected $fillable = ['id','title_th','tags_th','description_th','releaseDate','language','actor','duration','cover','contentUrl','status'];
    // protected $guarded = ['id'];
}
