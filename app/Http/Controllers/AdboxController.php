<?php

namespace App\Http\Controllers;

use App\Models\AdBox;
use DOMDocument;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Yajra\DataTables\Facades\DataTables;
use Intervention\Image\ImageManagerStatic as Image;
class AdboxController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('can:blogs-create')->only(['create', 'store']);
        $this->middleware('can:blogs-read')->only(['index', 'show']);
        $this->middleware('can:blogs-update')->only(['edit', 'update']);
        $this->middleware('can:blogs-delete')->only(['destroy', 'massDestroy']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $adboxs = AdBox::latest()->get();

        return view('adbox.index', compact('adboxs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('adbox.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'ads_name' => ['required'],
            'expires_at' => ['required'],
            'url_file' => ['required','image','mimes:jpeg,png,jpg,gif'],
        ]);

        $file = $request->file('url_file');
        $path = Storage::disk('public')->putFile('adbox_banner', new File($file));

        // return $imageName;
        
        $adBox = AdBox::create([
            'ads_name' => $request->ads_name,
            'status' => 'approved ads',
            'expires_at' => $request->expires_at,
            'url_file' => $path,
        ]);


        return redirect()->route('admin.adbox.index')->with('success', 'adbox uploaded !');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
     
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $adbox = AdBox::findOrFail($id);
        if (Storage::disk('public')->exists($adbox->url_file)) {
            Storage::disk('public')->delete($adbox->url_file);
        }
        $adbox->delete();

        return redirect()->route('admin.adbox.index')->with('success', 'adbox deleted !');
    }

    public function massDestroy(Request $request)
    {
        $arr = explode(',', $request->ids);

        foreach ($arr as $id) {
            $adbox = AdBox::findOrFail($id);
            if (Storage::disk('public')->exists($adbox->url_file)) {
                Storage::disk('public')->delete($adbox->url_file);
            }
            $adbox->delete();
        }

        return redirect()->route('admin.adbox.index')->with('success', 'Ads deleted !');
    }
}
