<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Movie;
use App\Models\Crawl;
use App\Models\AdBox;
use App\Models\AdContact;
use Illuminate\Support\Str;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Carbon\Carbon;

class HomeController extends Controller
{
    //
    public function home(){
        
        $url = env('API_BASE_URL');
        $token = env('API_TOKEN');
        $client = new Client(['base_uri' => $url]);
        $request = $client->request('GET',
        '/api/movies',['headers' => [ 'Authorization' => 'Bearer '.$token  ]]);
        $movies = collect(json_decode($request->getBody()->getContents(), true));
        $requestRandos = $client->request('GET',
        '/api/random-movies',['headers' => [ 'Authorization' => 'Bearer '.$token  ]]);
        $randos = collect(json_decode($requestRandos->getBody()->getContents(), true));
        $adboxs = AdBox::latest()->where('expires_at','>=',Carbon::now())->get();
        // $movies = array($movie);
        // return $url
        return view('IndexFirst',compact('movies','randos','adboxs'));
    }
    public function show($id)
    {
        $url = env('API_BASE_URL');
        $token = env('API_TOKEN');
        $client = new Client(['base_uri' => $url]);
        $requestRandos = $client->request('GET',
        '/api/random-movies',['headers' => [ 'Authorization' => 'Bearer '.$token  ]]);
        $randos = collect(json_decode($requestRandos->getBody()->getContents(), true));
        $movie = $client->request('GET','/api/movies/'.$id,['headers' => [ 'Authorization' => 'Bearer '.$token  ]])->getBody();
        // return $movie;
        return view('detailFilm',compact('randos'))->with('movie', json_decode($movie, true));
    }
    public function pages($current_page)
    {
        $url = env('API_BASE_URL');
        $token = env('API_TOKEN');
        $client = new Client(['base_uri' => $url]);
        $adboxs = AdBox::latest()->where('expires_at','>=',Carbon::now())->get();
        $movies = $client->request('GET','/api/movies?page='.$current_page,['headers' => [ 'Authorization' => 'Bearer '.$token  ]])->getBody();
        $requestRandos = $client->request('GET',
        '/api/random-movies',['headers' => [ 'Authorization' => 'Bearer '.$token  ]]);
        $randos = collect(json_decode($requestRandos->getBody()->getContents(), true));
        
        // return $movie;
        return view('pagesFilm',compact('adboxs','randos'))->with('movies', json_decode($movies, true));
    }
    public function search(Request $request){
        $request->validate([
            'title' => ['required', 'max:255'],
        ]);
        $title = $request->title;
        $url = env('API_BASE_URL');
        $token = env('API_TOKEN');
        $client = new Client(['base_uri' => $url]);
        $movies = $client->request('GET','/api/movies/search/'.$title,['headers' => [ 'Authorization' => 'Bearer '.$token  ]])->getBody();
        // return $movie;
        return view('SearchResult')->with('movies', json_decode($movies, true));
    }

    public function contact() {
        return view('Contact');
    }
    public function contactsubmit(Request $request) {
        $request->validate([
            'name' => ['required'],
            'email' => ['required'],
            'message' => ['required'],
           ]);

        $AdContact = AdContact::create([
            'name' => $request->name,
            'email' => $request->email,
            'message' => $request->message,
        ]);

        return redirect()->route('home')->with('success', 'Messages successfuly send !');
        ;
    }
}
