<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AdContact;
use Illuminate\Support\Str;
use Yajra\DataTables\Facades\DataTables;

class AdContactController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('can:blogs-create')->only(['create', 'store']);
        $this->middleware('can:blogs-read')->only(['index', 'show']);
        $this->middleware('can:blogs-update')->only(['edit', 'update']);
        $this->middleware('can:blogs-delete')->only(['destroy', 'massDestroy']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $adcontacts = AdContact::latest()->get();

        return view('adcontact.index', compact('adcontacts'));
    }
    
    public function show($id)
    {
        $adcontact = AdContact::findOrFail($id);

        return view('adcontact.show', compact('adcontact'));
    }
    public function destroy($id)
    {
        $adcontact = AdContact::findOrFail($id);
    
        $adcontact->delete();

        return redirect()->route('admin.adcontact.index')->with('success', 'Messages deleted !');
    }

    public function massDestroy(Request $request)
    {
        $arr = explode(',', $request->ids);

        foreach ($arr as $id) {
            $adcontact = AdContact::findOrFail($id);
            $adcontact->delete();
        }

        return redirect()->route('admin.adcontact.index')->with('success', 'Messages deleted !');
    }
}
