<?php

use App\Http\Controllers\AccountController;
use App\Http\Controllers\AdminIndex;
use App\Http\Controllers\BlogController;
use App\Http\Controllers\AdboxController;
use App\Http\Controllers\AdContactController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\HomeController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::get('/movie', [HomeController::class,'index'] )->name('movie');
// Route::get('/testmovie', [HomeController::class,'testPage'])->name('testmovie');
Route::get('/', [HomeController::class,'home'] )->name('home');
Route::get('/movies/{id}', [HomeController::class,'show'] )->name('show.movie');
Route::post('/movies/search', [HomeController::class,'search'] )->name('search.movie');
Route::get('/page/{current_page}', [HomeController::class,'pages'] )->name('pages.movie');
Route::get('/contact', [HomeController::class,'contact'])->name('contact');
Route::post('/contact', [HomeController::class,'contactsubmit'])->name('contact.submit');
// Route::get('/', function () {
//     return view('IndexFirst');
// })->name('landingpage');

Route::get('/dashboard', function () {
    return redirect()->route('admin.index');
})->middleware(['auth'])->name('dashboard');

// admin pages
Route::middleware(['auth', 'verified', 'can:admin-access'])->prefix('admin')->name('admin.')->group(function () {
    Route::get('/', AdminIndex::class)->name('index');

    // roles & permissions
    Route::resource('/permissions', PermissionController::class)->except(['show']);
    Route::resource('/roles', RoleController::class)->except(['show']);

    // users
    Route::resource('/users', UserController::class)->except(['show']);

    // blogs | comment this route below to disable Blog features
    Route::resource('/blogs', BlogController::class);
    Route::resource('/adbox', AdboxController::class);
    Route::resource('/adcontact', AdContactController::class);
   
    // bulk delete
    Route::delete('/bulk-delete/permissions', [PermissionController::class, 'massDestroy'])->name('permissions.massDestroy');
    Route::delete('/bulk-delete/roles', [RoleController::class, 'massDestroy'])->name('roles.massDestroy');
    Route::delete('/bulk-delete/users', [UserController::class, 'massDestroy'])->name('users.massDestroy');
    Route::delete('/bulk-delete/blogs', [BlogController::class, 'massDestroy'])->name('blogs.massDestroy');
    Route::delete('/bulk-delete/adbox', [AdboxController::class, 'massDestroy'])->name('adbox.massDestroy');
    Route::delete('/bulk-delete/adcontact', [AdContactController::class, 'massDestroy'])->name('adcontact.massDestroy');
    
});

// account re-verification
Route::middleware(['verified'])->group(function() {
    Route::get('account/verify-new-email/{token}', [AccountController::class, 'verifyNewEmail'])->name('account.verifyNewEmail');
    Route::resource('account', AccountController::class)->only(['index', 'edit', 'update']);
});

require __DIR__ . '/auth.php';
