<?php

namespace Database\Seeders;

use App\Models\Crawl;

use Illuminate\Database\Seeder;

class CrawlSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Crawl::create([
            'last_index_count' => '0',
        ]);
    }
}
