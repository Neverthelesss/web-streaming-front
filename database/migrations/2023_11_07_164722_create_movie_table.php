<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMovieTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movies', function (Blueprint $table) {
            // $table->id();
            $table->longtext('id');
            $table->longText('title_th');
            $table->longText('tags_th');
            $table->longText('title_en');
            $table->longText('tags_en');
            $table->longText('description_th');
            $table->longText('description_en');
            $table->longText('releaseDate');
            $table->longText('language');
            $table->longText('actor');
            $table->longText('duration');
            $table->longText('cover');
            $table->longText('contentUrl');
            $table->longText('status');
            $table->longText('sysCreateTime');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movies');
    }
}
