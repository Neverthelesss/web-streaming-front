@extends('layouts.movflx')
@section('content')
<section class="movie-details-area" data-background="{{$movie['cover']}}">
    <div class="container">
        <div class="row align-items-center position-relative ">
            <div class="col-xl-3 col-lg-4">
                <div class="movie-details-img">
                    <img src="{{$movie['cover']}}" alt="">
                    <a href="{{$movie['contentUrl']}}" class="popup-video"><img src="img/images/play_icon.png" alt=""></a>
                </div>
            </div>
            <div class="col-xl-6 col-lg-8 movie-details-prime">
                <div class="movie-details-content p-5" style="background: rgb(36,44,56);">
                    <h5>ตอนใหม่</h5>
                    <h2>{{$movie['title_th']}}</h2>
                    <div class="banner-meta">
                        <ul>
                            <li class="quality">
                                <span>Pg 18</span>
                                <span>hd</span>
                            </li>
                            <li class="category">
                                <a href="#">{{$movie['tags_th']}},</a>
                                
                            </li>
                            <li class="release-time">
                                <span><i class="far fa-calendar-alt"></i> {{$movie['releaseDate']}} </span>
                                <span><i class="far fa-clock"></i> {{$movie['duration']}} </span>
                            </li>
                        </ul>
                    </div>
                    <p>{{$movie['description_th']}}.</p>
                    <div class="movie-details-prime">
                        <ul>
                            <li class="streaming">
                                <h6>ไพรม์วิดีโอ</h6>
                                <span>ช่องสตรีมมิ่ง</span>
                            </li>
                            <li class="watch"><a href="{{$movie['contentUrl']}}" class="btn popup-video"><i class="fas fa-play"></i> ดูตอนนี้</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="ucm-area ucm-bg">
                <div class="container">
                    <div class="row align-items-end mb-55">
                        <div class="col-lg-12">
                            <div class="section-title text-center text-lg-center">
                                <span class="sub-title">สตรีมมิ่งออนไลน์</span>
                                <h2 class="title">สุ่มเลือกภาพยนตร์</h2>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                                
                        @foreach ($randos as $rando)
                        {{-- {{ $movie['title_th'] }} --}}
                        <div class="col-lg-3 col-sm-12 col-md-6">
                            <div class="movie-item mb-50">
                                <div class="movie-poster">
                                    <a href="{{ route('show.movie', $rando['id']) }}">
                                        <img src="{{ $rando['cover'] }}" alt=""></a>
                                </div>
                                <div class="movie-content">
                                    <div class="top">
                                        <h5 class="title">
                                            <a href="{{ route('show.movie', $rando['id']) }}">{{$rando['title_th']}}</a>
                                        </h5>
                                        <span class="date"> {{$rando['releaseDate']}} </span>
                                    </div>
                                    <div class="bottom">
                                        <ul>
                                            <li><span class="quality">hd</span></li>
                                            <li>
                                                <span class="duration"><i class="far fa-clock"></i> {{$rando['duration']}} </span>
                                                <span class="rating"><i class="fas fa-thumbs-up"></i> 3.5</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection