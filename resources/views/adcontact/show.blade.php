<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <div>
        <a href="{{route('admin.adcontact.index')}}">Back to Dashboard</a>
        <h1> Sender : {{$adcontact->name}} - {{$adcontact->email}} </h1>
        {!! $adcontact->message !!}
    </div>
</body>
</html>
