@extends('layouts.movflx')
@section('content')
    <!-- main-area -->
    <div>


        <!-- up-coming-movie-area -->
        <section class="ucm-area ucm-bg">
            {{-- <div class="ucm-bg-shape" data-background="img/bg/ucm_bg_shape.png"></div> --}}
            <div class="container">
                <div class="row align-items-end mb-55">
                    <div class="col-lg-12">
                        <div class="section-title text-center text-lg-center">
                            <span class="sub-title">สตรีมมิ่งออนไลน์</span>
                            <h2 class="title">ภาพยนตร์ล่าสุด</h2>
                        </div>
                    </div>
                    
                {{-- section ads box --}}
                <div class="row py-4">
                    @foreach ($adboxs as $adbox)
                    <div class="col-lg-6 col-sm-12">
                        <img style="object-fit: cover !important;width:650px !important;height:220px;" class="img-fluid" src="{{ Storage::disk('public')->url(@$adbox->url_file) }}" alt="{{$adbox->ads_name}}">
                    </div>
                    @endforeach
                </div>
                {{-- end section ads box --}}
                </div>
                <div class="row">
                    
                    @foreach ($movies['data'] as $movie)
                    {{-- {{ $movie['title_th'] }} --}}
                    <div class="col-lg-3 col-sm-12 col-md-6">
                        <div class="movie-item mb-50">
                            <div class="movie-poster">
                                <a href="{{ route('show.movie', $movie['id']) }}">
                                    <img src="{{ $movie['cover'] }}" alt=""></a>
                            </div>
                            <div class="movie-content">
                                <div class="top">
                                    <h5 class="title">
                                        <a href="{{ route('show.movie', $movie['id']) }}">{{$movie['title_th']}}</a>
                                    </h5>
                                    <span class="date"> {{$movie['releaseDate']}} </span>
                                </div>
                                <div class="bottom">
                                    <ul>
                                        <li><span class="quality">hd</span></li>
                                        <li>
                                            <span class="duration"><i class="far fa-clock"></i> {{$movie['duration']}} </span>
                                            <span class="rating"><i class="fas fa-thumbs-up"></i> 3.5</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                
            </div>
            {{-- @foreach ($movies['meta']['links'] as $links)
            <a href="{{ route('movie', $links['url']) }}">
                {!! $links['label'] !!}
            </a>
            @endforeach --}}
        </section>
        <center style="background:#000;padding:40px;">
            @if ($movies['prev_page_url'] != null)
            <span style="display: none;">{{$pageNumber = $movies['current_page'] - 1}}</span>

            <a style="z-index: 9999 !important;" class="btn border-t-cyan-50" href="{{ route('pages.movie', $pageNumber) }}">
                ก่อนหน้า
            </a>
            @endif
            @if ($movies['next_page_url'] != null)
            <span style="display: none;">{{$pageNumber = $movies['current_page'] + 1}}</span>

            <a style="z-index: 9999 !important;" class="btn border-t-cyan-50" href="{{ route('pages.movie', $pageNumber) }}">
                ต่อไป
                </a>
            @endif
        </center>
        <div class="ucm-area ucm-bg">
            <div class="container">
                <div class="row align-items-end mb-55">
                    <div class="col-lg-12">
                        <div class="section-title text-center text-lg-center">
                            <span class="sub-title">สตรีมมิ่งออนไลน์</span>
                            <h2 class="title">ภาพยนตร์สุ่มเลือก</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                            
                    @foreach ($randos as $rando)
                    {{-- {{ $movie['title_th'] }} --}}
                    <div class="col-lg-3 col-sm-12 col-md-6">
                        <div class="movie-item mb-50">
                            <div class="movie-poster">
                                <a href="{{ route('show.movie', $rando['id']) }}">
                                    <img src="{{ $rando['cover'] }}" alt=""></a>
                            </div>
                            <div class="movie-content">
                                <div class="top">
                                    <h5 class="title">
                                        <a href="{{ route('show.movie', $rando['id']) }}">{{$rando['title_th']}}</a>
                                    </h5>
                                    <span class="date"> {{$rando['releaseDate']}} </span>
                                </div>
                                <div class="bottom">
                                    <ul>
                                        <li><span class="quality">hd</span></li>
                                        <li>
                                            <span class="duration"><i class="far fa-clock"></i> {{$rando['duration']}} </span>
                                            <span class="rating"><i class="fas fa-thumbs-up"></i> 3.5</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>


    </div>
    <!-- main-area-end -->
@endsection