@extends('layouts.movflx')
@section('content')
    <!-- main-area -->
    <div>


        <!-- up-coming-movie-area -->
        <main>

            <!-- breadcrumb-area -->
            <section class="ucm-area ucm-bg" data-background="{{asset('movflx/img/bg/breadcrumb_bg.jpg')}}" style="background-image: url(&quot;{{asset('movflx/img/bg/breadcrumb_bg.jpg')}}&quot;);">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="breadcrumb-content">
                                <h2 class="title">ติดต่อเรา</h2>
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="{{route('home')}}">บ้าน</a></li>
                                        <li class="breadcrumb-item active" aria-current="page">ติดต่อ</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- breadcrumb-area-end -->

            <!-- contact-area -->
            <section class="ucm-area ucm-bg" data-background="{{asset('movflx/img/bg/contact_bg.jpg')}}" style="background-image: url(&quot;{{asset('movflx/img/bg/contact_bg.jpg')}}&quot;);">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-8 col-lg-7">
                            <div class="contact-form-wrap">
                                <div class="widget-title mb-50">
                                    <h5 class="title">แบบฟอร์มการติดต่อ</h5>
                                </div>
                                <div class="contact-form">
                                    <form action="{{route('contact.submit')}}" enctype="multipart/form-data" method="POST">
                                        @csrf
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="text" name="name" placeholder="ชื่อของคุณ *">
                                            </div>
                                            <div class="col-md-6">
                                                <input type="email" name="email" placeholder="อีเมลของคุณ *">
                                            </div>
                                        </div>
                                        <textarea name="message" placeholder="พิมพ์ข้อความของคุณ..."></textarea>
                                        <button class="btn">ส่งข้อความ</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-5">
                            <div class="widget-title mb-50">
                                <h5 class="title">ข้อมูล</h5>
                            </div>
                            <div class="contact-info-wrap">
                                <p><span>ข้อมูล :</span> หากท่านต้องการติดต่อลงโฆษณา กรุณากรอกแบบฟอร์มนี้</p>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- contact-area-end -->

        </main>


    </div>
    <!-- main-area-end -->
@endsection